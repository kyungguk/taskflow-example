#include <iostream>
#include <random>
#include <taskflow/taskflow.hpp>

namespace {
long expensive_calculation()
{
    std::mt19937                            rng{ std::random_device{}() };
    std::uniform_int_distribution<unsigned> dist;
    long                                    count = 0;
    for (int i = 0; i < 10000; ++i) { // NOLINT
        if (0 == dist(rng) % 2)       // NOLINT
            ++count;
    }
    return count;
}

[[nodiscard]] auto example_1()
{
    tf::Taskflow taskflow;

    // create four tasks
    auto [A, B, C, D] = taskflow.emplace(
        [] {
            std::cout << "TaskA\n";
        },
        [] {
            std::cout << "TaskB : " << expensive_calculation() << std::endl;
        },
        [] {
            std::cout << "TaskC : " << expensive_calculation() << std::endl;
        },
        [] {
            std::cout << "TaskD\n";
        });

    A.precede(B, C); // A runs before B and C
    D.succeed(B, C); // D runs after  B and C

    return taskflow;
}

[[nodiscard]] auto example_2()
{
    tf::Taskflow taskflow;

    auto A = taskflow.emplace([] {}).name("A"); // NOLINT
    auto C = taskflow.emplace([] {}).name("C"); // NOLINT
    auto D = taskflow.emplace([] {}).name("D"); // NOLINT

    auto B = taskflow.emplace([](tf::Subflow &subflow) { // subflow task B
                         auto B1 = subflow.emplace([] {
                                              std::cout << "TaskB1 : " << expensive_calculation() << std::endl;
                                          })
                                       .name("B1");
                         auto B2 = subflow.emplace([] {
                                              std::cout << "TaskB2 : " << expensive_calculation() << std::endl;
                                          })
                                       .name("B2");
                         auto B3 = subflow.emplace([] {}).name("B3");
                         B3.succeed(B1, B2); // B3 runs after B1 and B2
                     })
                 .name("B");

    A.precede(B, C); // A runs before B and C
    D.succeed(B, C); // D runs after  B and C

    return taskflow;
}

[[nodiscard]] auto example_3()
{
    tf::Taskflow taskflow;

    tf::Task init = taskflow.emplace([] {}).name("init");
    tf::Task stop = taskflow.emplace([] {}).name("stop");

    // creates a condition task that returns a random binary
    tf::Task cond = taskflow.emplace([] {
                                return std::rand() % 2;
                            })
                        .name("cond");

    // creates a feedback loop {0: cond, 1: stop}
    init.precede(cond);
    cond.precede(cond, stop); // moves on to 'cond' on returning 0, or 'stop' on 1

    return taskflow;
}
} // namespace

int main()
{
    tf::Executor executor{};

    executor.run(example_1());
    executor.run(example_2());
    executor.run(example_3());

    executor.wait_for_all();
    return 0;
}
